﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public Text WarningText;
    public PlayerHealth playerHealth;       
    public float restartDelay = 5f;
    bool gameOver = false;            


    Animator anim;                          
    float restartTimer;                    


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.CurrentHealth <= 0)
        {
            if(!gameOver) {
                anim.SetTrigger("GameOver");
                gameOver = true;
            }

            restartTimer += Time.deltaTime;

            if (restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    public void ShowWarning(float enemyDistance) {
        WarningText.text = $"WARNING\n{Mathf.RoundToInt(enemyDistance)} m";
        anim.SetTrigger("Warning");
    }
}