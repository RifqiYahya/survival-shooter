﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth PlayerHealth;
    // public GameObject Enemy;
    public float SpawnTime = 3f;
    public Transform[] SpawnPoints;

    [SerializeField]
    EnemyFactory _factory;


    void Start ()
    {
        //Execute spawn in certain time range spawn time
        InvokeRepeating("Spawn", SpawnTime, SpawnTime);
    }


    void Spawn ()
    {
        //Stopspawn if the player is dead
        if (PlayerHealth.CurrentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range (0, SpawnPoints.Length);
        int spawnNumber = Random.Range(0, _factory.EnemyPrefab.Length);

        _factory.FactoryMethod(spawnNumber, SpawnPoints[spawnPointIndex]);

        // Instantiate(Enemy, SpawnPoints[spawnPointIndex].position, SpawnPoints[spawnPointIndex].rotation);

    }
}
