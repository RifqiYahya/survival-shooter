﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int StartingHealth = 100;
    public int CurrentHealth;
    public Slider HealthSlider;
    public Image DamageImage;
    public AudioClip DeathClip;
    public float FlashSpeed = 5f;
    public Color FlashColour = new Color(1f, 0f, 0f, 0.1f);
    InputHandler _inputHandler;


    Animator _anim;
    AudioSource _playerAudio;
    PlayerMovement _playerMovement;
    PlayerShooting _playerShooting;
    bool _isDead;                                                
    bool _damaged;                                               


    void Awake()
    {
        _anim = GetComponent<Animator>();
        _playerAudio = GetComponent<AudioSource>();
        _playerMovement = GetComponent<PlayerMovement>();
        _playerShooting = GetComponentInChildren<PlayerShooting>();
        _inputHandler = GetComponent<InputHandler>();

        CurrentHealth = StartingHealth;
    }


    void Update()
    {
        //Give damaged effect if the player got hit
        if (_damaged)
        {
            DamageImage.color = FlashColour;
        }
        else
        {
            DamageImage.color = Color.Lerp(DamageImage.color, Color.clear, FlashSpeed * Time.deltaTime);
        }

        _damaged = false;
    }

    public void AddHealth (float bonus) {
        if (CurrentHealth + (int)bonus >= StartingHealth) {
            CurrentHealth = StartingHealth;
        } else {
            CurrentHealth += (int)bonus;
        }

        HealthSlider.value = CurrentHealth;
    }

    //Player get damage
    public void TakeDamage(int amount)
    {
        _damaged = true;

        CurrentHealth -= amount;

        HealthSlider.value = CurrentHealth;

        _playerAudio.Play();

        if (CurrentHealth <= 0 && !_isDead)
        {
            Death();
        }
    }


    void Death()
    {
        _isDead = true;

        _playerShooting.DisableEffects();

        //Play dead animation
        _anim.SetTrigger("Die");

        //Play dead audio
        _playerAudio.clip = DeathClip;
        _playerAudio.Play();

        //Disable Player Movement
        _playerMovement.enabled = false;
        _playerShooting.enabled = false;
        _inputHandler.enabled = false;
    }

    public void RestartLevel() {
        //Reload the scene
        SceneManager.LoadScene(0);
    }

}
