using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    public GameOverManager GameOverManager;
    
    void OnTriggerEnter(Collider collider) {
        if (collider.tag == "Enemy" && !collider.isTrigger) {
            float enemyDistance = Vector3.Distance(transform.position, collider.transform.position);
            GameOverManager.ShowWarning(enemyDistance);
        }
    }
}
