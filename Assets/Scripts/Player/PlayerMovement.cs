﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed = 6f;
    Vector3 _movement;
    Animator _anim;
    Rigidbody _playerRb;
    int _floorMask;
    float _camRayLength = 100f;
    RaycastHit _floorHit;

    void Awake() {
        // Obtain mask value from floor object
        _floorMask = LayerMask.GetMask("Floor");

        //Obtain animator component
        _anim = GetComponent<Animator>();

        //Obtain rigidbody component
        _playerRb = GetComponent<Rigidbody>();
    }

    void FixedUpdate() {
        //Obtain horizontal input
        float h = Input.GetAxisRaw("Horizontal");

        //Obtain vertical input
        float v = Input.GetAxisRaw("Vertical");

        // Move(h,v);
        Turning();
        // Animating(h, v);
    }

    void Turning() {
        //Create the ray
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Create the raycast hitting the floor
        // RaycastHit floorHit;

        //Do the raycasting
        if (Physics.Raycast(camRay,out _floorHit,_camRayLength,_floorMask)){
            //Onbtain the vector from player position to hit position
            Vector3 playerToMouse = _floorHit.point - transform.position;
            playerToMouse.y = 0f;

            //Set the quaternion based on look vector
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            //Rotate the player
            _playerRb.MoveRotation(newRotation);
        }
    }

    void OnDrawGizmos() {
        Gizmos.DrawSphere(_floorHit.point, 0.5f);
    }

    //Player move method
    public void Move(float h, float v) {
        //Set the value of x and y
        _movement.Set(h, 0f, v);

        //Normalize vector value to obtain length of 1
        _movement = _movement.normalized*Speed*Time.deltaTime;

        //Move the player
        _playerRb.MovePosition(transform.position + _movement);
    }

    //Animate
    public void Animating(float h, float v) {
        bool walking = h != 0f || v != 0f;
        _anim.SetBool("IsWalking", walking);
    }
}
