using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommandPattern {

    //Parent Class
    public abstract class Command
    {
        protected float moveDistance = 1f;

        public abstract void Execute(Transform boxTrans, Command command);

        public virtual void Undo(Transform boxTrans){}

        public virtual void Move(Transform boxTrans){}
    }

    //input handler
    public class InputHandler : MonoBehaviour
    {
        public Transform BoxTrans;

        Command buttonW, buttonS, buttonA, buttonD, buttonB, buttonZ, buttonR;

        public static List<Command> OldCommands = new List<Command>();

        Vector3 boxStartPos;

        Coroutine replayCoroutine;

        public static bool ShouldStartReplay;

        bool isReplaying;

        void Start() {
            //Bind keys with commands
            buttonB = new DoNothing();
            buttonW = new MoveForward();
            buttonS = new MoveReverse();
            buttonA = new MoveLeft();
            buttonD = new MoveRight();
            buttonZ = new UndoCommand();
            buttonR = new ReplayCommand();
        }

        void Update() {
            if(!isReplaying) {
                HandleInput();
            }

            StartReplay();
        }

        //Check key pressed if so, check what the key is binded to
        public void HandleInput() {
            if(Input.GetKeyDown(KeyCode.A)) {
                buttonA.Execute(BoxTrans, buttonA);
            }
            if(Input.GetKeyDown(KeyCode.B)) {
                buttonB.Execute(BoxTrans, buttonB);
            }
            if(Input.GetKeyDown(KeyCode.D)) {
                buttonD.Execute(BoxTrans, buttonD);
            }
            if(Input.GetKeyDown(KeyCode.R)) {
                buttonR.Execute(BoxTrans, buttonR);
            }
            if(Input.GetKeyDown(KeyCode.S)) {
                buttonS.Execute(BoxTrans, buttonS);
            }
            if(Input.GetKeyDown(KeyCode.W)) {
                buttonW.Execute(BoxTrans, buttonW);
            }
            if(Input.GetKeyDown(KeyCode.Z)) {
                buttonZ.Execute(BoxTrans, buttonZ);
            }
        }

        //Check whether replay is necessary
        void StartReplay() {
            if (ShouldStartReplay && OldCommands.Count > 0) {
                ShouldStartReplay = false;

                //stop the coroutine to start it from the beginning
                if(replayCoroutine != null) {
                    StopCoroutine(replayCoroutine);
                }

                //Start the replay
                replayCoroutine = StartCoroutine(ReplayCommands(BoxTrans));
            }
        }

        //Replay coroutine
        IEnumerator ReplayCommands(Transform boxTrans) {
            //prevent moving box while replaying
            isReplaying = true;

            boxTrans.position = boxStartPos;

            for (int i = 0; i < OldCommands.Count; i++) {
                //move the box with current command
                OldCommands[i].Move(boxTrans);

                yield return new WaitForSeconds(0.3f);
            }

            isReplaying = false;
        }
    }

    //Child Classes
    public class MoveForward : Command
    {
        public override void Execute(Transform boxTrans, Command command)
        {
            Move(boxTrans);

            InputHandler.OldCommands.Add(command);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.forward*moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.forward*moveDistance);
        }
    }

    public class MoveReverse : Command {
        public override void Execute(Transform boxTrans, Command command)
        {
            Move(boxTrans);

            InputHandler.OldCommands.Add(command);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.forward*moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.forward*moveDistance);
        }
    }

    public class MoveLeft : Command {
        public override void Execute(Transform boxTrans, Command command)
        {
            Move(boxTrans);

            InputHandler.OldCommands.Add(command);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.right*moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.right*moveDistance);
        }
    }

    public class MoveRight : Command {
        public override void Execute(Transform boxTrans, Command command)
        {
            Move(boxTrans);

            InputHandler.OldCommands.Add(command);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.right*moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.right*moveDistance);
        }
    }

    //for keys with no binding
    public class DoNothing : Command{
        public override void Execute(Transform boxTrans, Command command)
        {
            return;
        }
    }

    //Undo one command
    public class UndoCommand : Command {
        public override void Execute(Transform boxTrans, Command command)
        {
            List<Command> oldCommands = InputHandler.OldCommands;

            if(oldCommands.Count > 0) {
                Command latestCommand = oldCommands[oldCommands.Count - 1];

                latestCommand.Undo(boxTrans);

                oldCommands.Remove(latestCommand);
            }
        }
    }

    //Replay all commands
    public class ReplayCommand : Command {
        public override void Execute(Transform boxTrans, Command command)
        {
            InputHandler.ShouldStartReplay = true;
        }
    }

   
}
