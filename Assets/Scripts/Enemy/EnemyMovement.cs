﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform _player;
    PlayerHealth _playerHealth;
    EnemyHealth _enemyHealth;
    NavMeshAgent _nav;


    private void Awake ()
    {
        //FInding the player object
        _player = GameObject.FindGameObjectWithTag ("Player").transform;

        //Get the player health component
        _playerHealth = _player.GetComponent <PlayerHealth> ();

        //Get the enemy health
        _enemyHealth = GetComponent <EnemyHealth> ();

        //Get the navmesh agent
        _nav = GetComponent <NavMeshAgent> ();
    }


    void Update ()
    {
        if (_playerHealth.CurrentHealth > 0 && _enemyHealth.CurrentHealth > 0)
        {
            _nav.SetDestination (_player.position);
        }
        else
        {
           _nav.enabled = false;
        }
    }
}
