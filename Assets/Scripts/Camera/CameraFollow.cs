using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public float Smoothing = 5f;
    Vector3 _offset;
    
    // Start is called before the first frame update
    void Start()
    {
        //Obtain the offset between the target and cam
        _offset = transform.position - Target.position;    
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 targetCamPos = Target.position + _offset;

        //set the camera position with smoothing
        transform.position = Vector3.Lerp(transform.position, targetCamPos, Smoothing*Time.deltaTime);
    }
}
