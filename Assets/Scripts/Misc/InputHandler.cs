using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public PlayerShooting playerShooting;
    public float maxCommandHistory = 100;

    //Command queue list
    List<Command> commands = new List<Command>();

    void FixedUpdate() {
        //Menghandle input movement
        Command moveCommand = InputMovementHandling();
        if (moveCommand != null) {
            commands.Add(moveCommand);
            moveCommand.Execute();
        }

        if (commands.Count > 100) {
            commands.RemoveAt(0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Handing shoot
        Command shootCommand = InputShootHandling();

        playerShooting.timer += Time.deltaTime;

        if (shootCommand != null) {
            shootCommand.Execute();
        }

        if (playerShooting.timer >= playerShooting.TimeBetweenBullets*playerShooting.effectsDisplayTime) {
            playerShooting.DisableEffects();
        }
    }

    Command InputMovementHandling() {

        if (Input.GetKey(KeyCode.Z)) {
            return Undo();
        } else {
            return new MoveCommand(playerMovement, HorizontalInput(), VerticalInput());
        }
    }

    float HorizontalInput() {
        if (Input.GetKey(KeyCode.D)) {
            return 1f;
        } else if (Input.GetKey(KeyCode.A)) {
            return -1f;
        }

        return 0f;
    }

    float VerticalInput() {
        if (Input.GetKey(KeyCode.W)) {
            return 1f;
        } else if (Input.GetKey(KeyCode.S)) {
            return -1f;
        }

        return 0f;
    }

    Command Undo() {
        //If queue is not empty do the undo
        if (commands.Count > 0) {
            Command undoCommand = commands[commands.Count-1];
            undoCommand.UnExecute();
            commands.RemoveAt(commands.Count-1);
        }
        return null;
    }

    Command InputShootHandling() {
        //If shoot trigger shoot command
        if(Input.GetButton("Fire1") && playerShooting.timer > playerShooting.TimeBetweenBullets) {
            return new ShootCommand(playerShooting);
        } else {
            return null; 
        }
    }
}
