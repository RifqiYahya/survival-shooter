using System;
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactory
{
    [SerializeField]
    public GameObject[] EnemyPrefab;
    
    public GameObject FactoryMethod(int tag, Transform loc) {
        GameObject enemy = Instantiate(EnemyPrefab[tag], loc.position, loc.rotation);

        return enemy;
    }
}
