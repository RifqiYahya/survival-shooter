public abstract class Command
{
    public abstract void Execute();
    public abstract void UnExecute();
}

public class MoveCommand : Command {
    PlayerMovement playerMovement;
    float h, v;

    public MoveCommand(PlayerMovement _playerMovement, float _h, float _v) {
        playerMovement = _playerMovement;
        h = _h;
        v = _v;
    }

    //Trigger movement command
    public override void Execute()
    {
        playerMovement.Move(h, v);
        //Animate player
        playerMovement.Animating(h, v);
    }

    public override void UnExecute()
    {
        //Invert player movement
        playerMovement.Move(-h, -v);

        //Animate
        playerMovement.Animating(h, v);
    }
}

public class ShootCommand : Command {
    PlayerShooting playerShooting;

    public ShootCommand(PlayerShooting _playerShooting) {
        playerShooting = _playerShooting;
    }

    public override void Execute()
    {
        //shoot
        playerShooting.Shoot();
    }

    public override void UnExecute()
    {
        return;
    }
}
