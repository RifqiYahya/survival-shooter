using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class BonusController : MonoBehaviour
{
    public enum BonusType {health, speed}

    Animator Anim;

    [HideInInspector]
    public bool obtained;

    public BonusType bonusType;

    public float healthBonus = 20f;
    public float speedBonus = 2f;
    public float speedBonusDuration = 5f;


    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
    }

    void OnTriggerEnter (Collider collider) {
        if (collider.tag == "Player" && !obtained && !collider.isTrigger) {
            obtained = true;
            AddBonus();

            //PlayAnimation
            Anim.SetTrigger("Obtained");
        }
    }

    void AddBonus() {
        if (bonusType == BonusType.health) {
            BonusManager.instance.AddHealthBonus(healthBonus);
        } else if (bonusType == BonusType.speed) {
            BonusManager.instance.AddSpeedBonus(speedBonus, speedBonusDuration);
        }
    }

    // #if UNITY_EDITOR
    // [CustomEditor(typeof(BonusController))]
    // public class BonusEditor : Editor {
    //     public override void OnInspectorGUI()
    //     {
    //         base.OnInspectorGUI();

    //         BonusController Bonus = target as BonusController;

    //         if (Bonus.bonusType == BonusType.health) {
    //             Bonus.healthBonus = EditorGUILayout.FloatField("Health Bonus",Bonus.healthBonus);
    //         } else if (Bonus.bonusType == BonusType.speed) {
    //             Bonus.speedBonus = EditorGUILayout.FloatField("Speed Bonus", Bonus.speedBonus);
    //             Bonus.speedBonusDuration = EditorGUILayout.FloatField("Bonus Duration", Bonus.speedBonusDuration);
    //         }
    //     }
    // }

    // #endif


}
