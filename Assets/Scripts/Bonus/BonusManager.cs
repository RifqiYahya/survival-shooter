using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusManager : MonoBehaviour
{
    static BonusManager _instance;
    public static BonusManager instance {
        get{
            if (_instance == null) {
                _instance = FindObjectOfType<BonusManager>();
            }

            return _instance;
        }
    }

    public GameObject Player;
    PlayerHealth playerHealth;
    PlayerMovement playerMovement;

    public Text BonusCounter;

    public GameObject instanceParent;
    public List<Transform> SpawnPoints;
    public BonusController[] BonusPrefabs;
    List<BonusController> SpawnedBonus;

    float _timer;
    float _speedBonus;
    float _initSpeed;

    // Start is called before the first frame update
    void Start()
    {
        playerHealth = Player.GetComponent<PlayerHealth>();
        playerMovement = Player.GetComponent<PlayerMovement>();
        foreach(Transform sp in SpawnPoints) {
            int rnd = Random.Range(-1, BonusPrefabs.Length);

            if (rnd > -1) {
                BonusController bonus =  Instantiate(BonusPrefabs[rnd].gameObject, instanceParent.transform).GetComponent<BonusController>();
                bonus.transform.position = sp.position;
            }
        }

        _initSpeed = playerMovement.Speed;
    }

    // Update is called once per frame
    void Update() {
        if (_timer > 0) {
            playerMovement.Speed = _initSpeed + _speedBonus;
            BonusCounter.gameObject.SetActive(true);
            BonusCounter.text = _timer.ToString("N2");
            _timer -= Time.deltaTime;
            return;
        }
        BonusCounter.gameObject.SetActive(false);
        playerMovement.Speed = _initSpeed;
    }

    public void AddHealthBonus(float bonus) {
        playerHealth.AddHealth(bonus);
    }

    public void AddSpeedBonus(float bonus, float time) {
        _speedBonus = bonus;
        _timer = time;
    }
}
